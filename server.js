// server.js
// load the things we need
var express = require('express');
var app = express();
require('express-helpers')(app);
// set the view engine to ejs
app.set('view engine', 'ejs');

// use res.render to load up an ejs view file

app.use(express.static('libs'))

var lessons = [
    		{id:'vjoneirn9',name:'A',teacher:'B',start_date:'2010/1/21',finish_date:'2011/1/2',status:'ongoing',type:'lesson'},
    		{id:'vjoneirn9',name:'AA',teacher:'B',start_date:'2010/1/1',finish_date:'2011/1/1',status:'ongoing',type:'template'},
    		{id:'vjoneirn9',name:'AAA',teacher:'BB',start_date:'2010/1/1',finish_date:'2011/1/1',status:'ongoing',type:'template'}
    	];
// index page 
app.get('/', function(req, res) {
    res.render('pages/index',{
    	lessons: lessons
    });
});

// about page 
app.get('/about', function(req, res) {
    res.render('pages/about');
});

app.get('/lesson/vjoneirn9',function(req, res) {
	res.render('pages/lesson',{
		info:lessons[0],
		q1:{
			questions:[
				{
					q:"How are you",
					a:["good","ok","bad"],
					type:"multi"
				},
				{
					q:"How old are you",
					a:["no good","no ok","no bad"],
					type:"multi"
				}
			]
		}
	});
})

app.listen(8080);
console.log('8080 is the magic port');